<?php

namespace Sebwite\Filesystem;

use Sebwite\Support\ServiceProvider;

/**
* The main service provider
*
* @author        Sebwite
* @copyright  Copyright (c) 2015, Sebwite
* @license      http://mit-license.org MIT
*/
class FilesystemServiceProvider extends ServiceProvider
{
    protected $dir = __DIR__;

    protected $configFiles = [ 'sebwite.filesystem' ];

    protected $weaklings = [

    ];

    protected $aliases = [

    ];

    protected $singletons = [
        'fs' => Filesystem::class
    ];


}
